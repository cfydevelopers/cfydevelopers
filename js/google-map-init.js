jQuery(document).ready(function() {

    var centerLocation = new google.maps.LatLng(32.7765, -79.9311);

    function initializeMap() {

      var mapProperties = {
        center: centerLocation,
        zoom: 12,
        scrollwheel: false,
        draggable: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };

      var map = new google.maps.Map(document.getElementById("map"),mapProperties);

      var pinMarker = new google.maps.Marker({
        position: centerLocation,
      });

      pinMarker.setMap(map);

    }

    google.maps.event.addDomListener(window, 'load', initializeMap);

});
